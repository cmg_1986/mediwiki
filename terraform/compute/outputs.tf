output "elb-dns-name" {
  value = "${aws_elb.media-wiki-elb.dns_name}"
}